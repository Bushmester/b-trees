import os
import zipfile

import dropbox

from config import TOKEN_DROPBOX

dbx = dropbox.Dropbox(TOKEN_DROPBOX)


if not os.path.exists(os.path.join('tests')):
    os.mkdir('tests')

if len(os.listdir('tests')) == 0:
    with open(file=os.path.join('tests', 'tests.zip'), mode='wb+') as file:
        metadata, res = dbx.files_download(path="/apps/tests_for_semestr/tests.zip")
        file.write(res.content)

    z_file = zipfile.ZipFile(file=os.path.join('tests', 'tests.zip'))
    z_file.extractall(path=os.path.join('tests'))

    os.remove(path=os.path.join('tests', 'tests.zip'))
