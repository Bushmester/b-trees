#include <iostream>
#include "BTree.hpp"


BTreeNode::BTreeNode(int degree, bool isLeaf){
    this->degree = degree;
    this->isLeaf = isLeaf;

    this->keys = new int[2 * this->degree - 1];
    this->child = new BTreeNode *[2 * this->degree];

    this->currentNumberOfKeys = 0;
}


void BTreeNode::print() {
    int i;



    for (i=0; i < this->currentNumberOfKeys; i++) {
        if (this->isLeaf == false) {
            this->child[i]->print();
        }

        std::cout << " " << this->keys[i];
    }

    if (this->isLeaf == false) {
            this->child[i]->print();
    }
}


bool BTreeNode::search(int key) {
    int i = 0;

    while(i < this->currentNumberOfKeys && key > this->keys[i]) {
        i++;
    }

    if (this->keys[i] == key) {
        return true;
    }

    if (this->isLeaf == true) {
        return false;
    }

    return this->child[i]->search(key);
}


void BTreeNode::insertNonFull(int key) {
    int i = this->currentNumberOfKeys - 1;

    if (this->isLeaf == true) {
        while (i >= 0 && this->keys[i] > key) {
            this->keys[i + 1] = this->keys[i];
            i--;
        }

        this->keys[i + 1] = key;
        this->currentNumberOfKeys += 1;
    }
    else {
        while (i >= 0 && this->keys[i] > key) {
            i--;
        }

        if (this->child[i + 1]->currentNumberOfKeys == 2 * this->degree - 1) {
            splitChild(i + 1, this->child[i + 1]);

            if (this->keys[i + 1] < key) {
                i++;
            }
        }
        this->child[i + 1]->insertNonFull(key);
    }
}


void BTreeNode::splitChild(int i, BTreeNode* secondChild) {
    BTreeNode* newChild = new BTreeNode(secondChild->degree, secondChild->isLeaf);
    newChild->currentNumberOfKeys = this->degree - 1;

    for (int j=0; j < this->degree - 1; j++) {
        newChild->keys[j] = secondChild->keys[j + this->degree];
    }

    if (secondChild->isLeaf == false) {
        for (int j=0; j < this->degree; j++) {
            newChild->child[j] = secondChild->child[j + this->degree];
        }
    }

    secondChild->currentNumberOfKeys = this->degree - 1;
    for (int j=this->currentNumberOfKeys; j >= i + 1; j--) {
        this->child[j + 1] = this->child[j];
    }

    this->child[i + 1] = newChild;

    for (int j=this->currentNumberOfKeys - 1; j >= i; j--) {
        this->keys[j + 1] = this->keys[j];
    }

    this->keys[i] = secondChild->keys[degree - 1];
    this->currentNumberOfKeys += 1;
}


BTree::BTree(int degree) {
    this->root = nullptr;
    this->degree = degree;
}


void BTree::print() {
    if (this->root != nullptr) {
        this->root->print();
    }
}


bool BTree::search(int key) {
    return (this->root == nullptr) ? false : this->root->search(key);
}


void BTree::insert(int key) {
    if (this->root == nullptr) {
        this->root = new BTreeNode(this->degree, true);
        this->root->keys[0] = key;
        this->root->currentNumberOfKeys = 1;
    }
    else {
        if (this->root->currentNumberOfKeys == 2 * this->degree - 1) {
            int i = 0;
            BTreeNode* newNode = new BTreeNode(this->degree, false);

            newNode->child[0] = this->root;

            newNode->splitChild(0, this->root);

            if (newNode->keys[0] < key) {
                i++;
            }
            newNode->child[i]->insertNonFull(key);

            this->root = newNode;
        }
        else {
            this->root->insertNonFull(key);
        }
    }
}
