#pragma once


class BTreeNode{
    int degree;
    bool isLeaf;

    int* keys;
    BTreeNode **child;

    int currentNumberOfKeys;

private:
    void insertNonFull(int key); // private?
    void splitChild(int i, BTreeNode* secondChild); // private?

public:
    BTreeNode(int degree, bool isLeaf);

    void print();
    bool search(int key);

friend class BTree;
};


class BTree{
    int degree;
    BTreeNode* root;

public:
    BTree(int degree);

    void print();

    bool search(int key);
    void insert(int key);
};
