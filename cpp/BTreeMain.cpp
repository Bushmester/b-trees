#include <iostream>
#include "shared/BTree.hpp"


int main() {
    BTree tree = BTree(3);
    tree.insert(8);
    tree.insert(9);
    tree.insert(10);
    tree.insert(11);
    tree.insert(15);
    tree.insert(16);
    tree.insert(17);
    tree.insert(18);
    tree.insert(20);
    tree.insert(23);

    std::cout << "The B-tree is: ";
    tree.print();

    std::cout << "" << std::endl;
    std::cout << tree.search(2) << std::endl;
    std::cout << tree.search(12) << std::endl;
    // int n;
    // std::cin >> n;

    // for (int i=0; i < n; i++) {
    //     int num;
    //     std::cin >> num;
    //     tree.insert(num);
    // }

    // tree.print();

    return 0;
}
